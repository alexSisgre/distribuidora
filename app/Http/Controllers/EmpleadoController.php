<?php

namespace App\Http\Controllers;

use App\Empleado;
use Illuminate\Http\Request;
use App\Http\Requests\EmpleadoRequest;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $empleados = Empleado::paginate(25);
        return "moment";
        //return view('empleados.index', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('empleados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpleadoRequest $request)
    {
        //
        //$empleado = Empleado::create($request->all());

        //$datosEmpelado = request()->all();

        $datosEmpelado = request()->except('_token','pais');

        // Normalizacion de ruta para archivo foto
        if($request->hasFile('foto')){
            $datosEmpelado['foto'] = $request->file('foto')->store('uploads','public');
        }

        if($request->hasFile('hv_documento')){
            $datosEmpelado['hv_documento'] = $request->file('hv_documento')->store('uploads','public');
        }

        if($request->hasFile('anexos')){
            $datosEmpelado['anexos'] = $request->file('anexos')->store('uploads','public');
        }

        Empleado::insert($datosEmpelado);

        echo "<pre>";
        print_r($datosEmpelado);
        echo "<pre>";

        return response()->json($datosEmpelado);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Empleado $empleado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleado $empleado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empleado $empleado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleado $empleado)
    {
        //
    }
}
