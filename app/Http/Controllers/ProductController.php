<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Categoria;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::join('categorias', 'categorias.id', '=', 'products.id_categoria')
        ->select('products.id as id_product',
                'categorias.nombre as nombre_categoria',
                'products.nombre as nombre_producto',
                'products.descripcion as producto_descripcion','precio','stock',
                'codigo_barras')
        ->paginate();
       
        $categorias = Categoria::all();


        return view('products.index', compact('products'), compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::all();

        return view('products.create',compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        if($request->form_index){
           
            if($request->categoria){
                $products = Product::join('categorias', 'categorias.id', '=', 'products.id_categoria')
                ->select('products.id as id_product',
                        'categorias.nombre as nombre_categoria',
                        'products.nombre as nombre_producto',
                        'products.descripcion as producto_descripcion',
                        'codigo_barras'
                    )
                ->where('categorias.id', '=', $request->categoria)
                ->paginate();
            
                $categorias = Categoria::all();
        
                return view('products.index', compact('products'), compact('categorias'));
            }elseif($request->producto){
                $products = Product::join('categorias', 'categorias.id', '=', 'products.id_categoria')
                ->select('products.id as id_product',
                        'categorias.nombre as nombre_categoria',
                        'products.nombre as nombre_producto',
                        'products.descripcion as producto_descripcion',
                        'codigo_barras','products.precio as precio','products.stock as stock'
                    )
                ->where('products.nombre', 'like', "%$request->producto%")
                ->paginate();
            
                $categorias = Categoria::all();
        
                return view('products.index', compact('products'), compact('categorias'));
            }
        }    
        (int)$request->get('id_categoria');
        $newRequest = $request->except('_token');
        
        $product = new Product();
        $product->id_categoria = $request->id_categoria;
        $product->codigo_barras = $request->codigo_barras;
        $product->nombre = $request->nombre;
        $product->descripcion = $request->descripcion;
        $product->precio = $request->precio;
        $product->stock = $request->stock;
        $product->save();


        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categorias = Categoria::all();

        return view('products.edit', compact('product','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::update($request->all());

        return redirect()->route('products.edit', $product->id)
            ->with('info', 'Rol guardado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id)->delete();

        return back()->with('info', 'Eliminado correctamente');
    }
}
