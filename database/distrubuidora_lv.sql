-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 06-05-2020 a las 17:01:35
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `epicodb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `fecha_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `descripcion`, `fecha_creacion`) VALUES
(1, 'Categoria 1', 'Grupo de productos en primera categoria', '2020-05-05 16:04:05'),
(2, 'Categoria 2', 'Grupo de productos en segunda categoria', '2020-05-05 16:04:05'),
(3, 'Categoria 3', 'Grupo de productos en tercera categoria', '2020-05-05 16:04:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(19, '2014_10_12_000000_create_users_table', 1),
(20, '2014_10_12_100000_create_password_resets_table', 1),
(21, '2015_01_20_084450_create_roles_table', 1),
(22, '2015_01_20_084525_create_role_user_table', 1),
(23, '2015_01_24_080208_create_permissions_table', 1),
(24, '2015_01_24_080433_create_permission_role_table', 1),
(25, '2015_12_04_003040_add_special_role_column', 1),
(26, '2017_10_17_170735_create_permission_user_table', 1),
(27, '2018_01_16_214348_create_products_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Navegar usuarios', 'users.index', 'Lista y navega todos los usuarios del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(2, 'Ver detalle de usuario', 'users.show', 'Ve en detalle cada usuario del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(3, 'Edición de usuarios', 'users.edit', 'Podría editar cualquier dato de un usuario del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(4, 'Eliminar usuario', 'users.destroy', 'Podría eliminar cualquier usuario del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(5, 'Navegar roles', 'roles.index', 'Lista y navega todos los roles del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(6, 'Ver detalle de un rol', 'roles.show', 'Ve en detalle cada rol del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(7, 'Creación de roles', 'roles.create', 'Podría crear nuevos roles en el sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(8, 'Edición de roles', 'roles.edit', 'Podría editar cualquier dato de un rol del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(9, 'Eliminar roles', 'roles.destroy', 'Podría eliminar cualquier rol del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(10, 'Navegar productos', 'products.index', 'Lista y navega todos los productos del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(11, 'Ver detalle de un producto', 'products.show', 'Ve en detalle cada producto del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(12, 'Creación de productos', 'products.create', 'Podría crear nuevos productos en el sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(13, 'Edición de productos', 'products.edit', 'Podría editar cualquier dato de un producto del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(14, 'Eliminar productos', 'products.destroy', 'Podría eliminar cualquier producto del sistema', '2019-09-04 00:51:32', '2019-09-04 00:51:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `codigo_barras` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` bigint(15) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `id_categoria`, `codigo_barras`, `nombre`, `descripcion`, `precio`, `stock`, `created_at`, `updated_at`) VALUES
(2, 1, 'laskdjksjkskddu', 'Quis soluta et est deleniti culpa aut aperiam et.', 'Vitae quam nihil in eligendi et vero tempora.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(3, 1, 'ccukspcicjalsic', 'Officiis ut pariatur modi excepturi autem odio voluptate molestiae.', 'Eius et non sed alias.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(4, 1, 'clslkcicyasyd', 'Fugit commodi enim voluptates.', 'Provident repellat voluptatum ipsum impedit.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(5, 1, 'clsisycyalapd', 'Ea enim aut sed laborum ea accusamus quam.', 'Distinctio eius ut alias illum quaerat dolorum nobis.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(6, 1, 'paocoisaufncoo', 'Aut maiores facere porro ut nemo porro illo.', 'Ut ab iste possimus consequatur qui.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(7, 1, 'alciuahpcociall', 'Eveniet voluptatem enim saepe.', 'Sed molestiae sed voluptas quam praesentium.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(8, 2, '', 'Eos aperiam est voluptatibus saepe aperiam omnis eveniet.', 'Sed quia ut asperiores similique modi nisi.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(9, 2, '', 'Cum laudantium consequatur quo tempora dolore consectetur.', 'Possimus illum voluptas modi quidem et sint ut ut.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(10, 2, '', 'Natus error debitis qui velit temporibus dolores saepe et.', 'Voluptates dolorem dolores provident voluptate ullam et non.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(11, 1, '', 'Omnis adipisci hic quod cumque.', 'Ipsam et laboriosam nam dolor.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(12, 3, '', 'Hic impedit deserunt sed qui dolores rerum aut.', 'Eos aliquid debitis qui inventore nisi quas.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(13, 1, '', 'Natus nostrum error veritatis sed corrupti non quas.', 'Dolores doloribus dignissimos qui non quod officiis.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(14, 3, '', 'Quia fugiat qui eum quo occaecati.', 'Velit soluta laborum esse ea.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(15, 2, '', 'Et impedit qui ea.', 'Ex voluptates temporibus dolore ut.', 0, 0, '2019-09-04 00:51:32', '2019-09-04 00:51:32'),
(100, 3, 'alsids88272j', 'zapatos adidas', 'ingreso de producto nuevo', 85000, 200, '2020-05-06 03:16:34', '2020-05-06 03:16:34'),
(101, 1, 'liodcosis882', 'Tennis Lacoste', 'Producto nuevo', 76000, 150, '2020-05-06 03:18:17', '2020-05-06 03:18:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `special` enum('all-access','no-access') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`, `special`) VALUES
(1, 'Admin', 'slug', NULL, '2019-09-04 05:51:32', '2019-09-04 05:51:32', 'all-access');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'pedro alexander rojas avila', 'pedrroja@gmail.com', '$2y$10$BSRhswvL.8cvsNidnpP6/O.rUEQYlJU.eNQpdxFLq/3vQXxHN8d6G', NULL, '2019-09-04 01:10:30', '2019-09-04 01:10:30');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
