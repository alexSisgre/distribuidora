
<head>
    <!-- for Login page we are added -->
    <!--<link href="../../libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
     <!-- Bootstrap 3.3.5 -->
     <link rel="stylesheet" href="{{ asset('libraries/bootstrap/css/bootstrap_3.3.5.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <!--<link href="{{ asset('libraries/bootstrap/css/bootstrap-responsive.min.css') }} " rel="stylesheet">--> 
    <link href="{{ asset('libraries/bootstrap/css/jqueryBxslider.css') }}" rel="stylesheet" />
    <script src="{{ asset('libraries/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('libraries/jquery/boxslider/jqueryBxslider.js') }}"></script>
    <script src="{{ asset('libraries/jquery/boxslider/respond.min.js') }}"></script>

</head>

    <section class="container" style="height:100%"> 
        <div class="row">
            <div class="pull-right footer-icons"> 
                
            </div>
        </div>
        <br><br><br><br><br>
        <div class="row" >
            <div class="login-box" >
                <div style="text-align:center">
                    <h3>Distribuidora</h3>
                    <h6></h6>
                </div>
                <!-- /.login-logo -->
                <div class="login-box-body" style="width:100%">
                    <p class="login-box-msg">Logueate para iniciar sesion</p>

                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <label for="email" class="col-md-3 control-label" style="margin-top:-11px">Correo Electronico</label>

                          <div class="col-md-8">
                              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                              @if ($errors->has('email'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <label for="password" class="col-md-3 control-label">Contraseña</label>

                          <div class="col-md-8">
                              <input id="password" type="password" class="form-control" name="password" required>

                              @if ($errors->has('password'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-4">
                            <div class="col-md-4 col-md-offset-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordar
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-5">
                            <div class="col-md-5 col-md-offset-9">
                                <button type="submit" class="btn btn-primary">
                                    Ingresar
                                </button>
                            </div>
                        </div>

                        <div class="col-md-12">
                          <a href="index.php?r=site/requestpasswordreset">Olvide mi contraseña</a><br>
                          <a href="index.php?r=site/signup" class="text-center">Solicitar registro de usuario</a>

                        </div>
                      </div>  
                    </form>
                  
                </div>
                <!-- /.login-box-body -->
            </div><!-- /.login-box -->
        </div> <!-- Fin Row --> 
        
        <hr style="border:1px solid #ddd;">

    </section> <!-- Fin container --> 

    <!-- Footer -->     
    <!-- <footer class="span6 pull-right" > -->

    <div class="navbar navbar-fixed-bottom">
        <div class="navbar-inner">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span6 pull-left" >
                        <div class="footer-content">
                             <small>Desarrollado por <a href="#" >Alexander Rojas</a>&nbsp; 
                                <!--<a href="https://www.vtiger.com"> vtiger.com</a> | 
                                <a href="https://www.vtiger.com/LICENSE.txt">Read License</a> | 
                                <a href="https://www.vtiger.com/products/crm/privacy_policy.html">Privacy Policy</a> </small>-->
                        </div>
                    </div>
                    <div class="span6 pull-right" >
                        <div class="pull-right footer-icons">
                            <small>Nuestras redes sociales&nbsp;</small>
                            <a href="#"><img width="30px" height="30px" src="{{ asset('libraries/img/facebook.jpg') }}"></a>
                            &nbsp;<a href="#"><img width="30px" height="30px" src="{{ asset('libraries/img/fotos.jpg') }}"></a> 
                            &nbsp;<a href="#"><img width="30px" height="30px" src="{{ asset('libraries/img/bird.jpg') }}"></a>
                            &nbsp;<a href="#"><img width="30px" height="30px" src="{{ asset('libraries/img/google.png') }}"></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
                        </div>
                    </div>
                </div>   
            </div>    
        </div>   
    </div>