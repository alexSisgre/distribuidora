@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Productos
        <small>- inicio</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Productos</li>
        </ol>
    </section>

 
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <!-- small box -->
                <div class="tab-content">
        
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa"></i>
                            <h3 class="box-title">Gestion de Productos.</h3>

                            <div class="box-tools pull-right">
                                @can('products.create')
                                <a id="deleteProducto" href="{{ route('products.create') }}" 
                                    class="btn btn-success pull-right">
                                    Crear Producto
                                </a>
                                @endcan
                            </div>
                        </div>
                        
                        <!-- /.box-header -->
                        <div class="box-body">    
                            <div class="col-md-8 ">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <i class="fa fa-search"></i>
                                        <h6 class="box-title" style="font-size:14px">Filtrar por.</h6>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>    
                                
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="form-group col-md-6">
                                            <label for="name">Categoria</label>
                                            <input class="form-control" id="name" placeholder="Nombre Usuario" name="name" type="text">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="name">Nombre Producto</label>
                                            <input class="form-control" id="email" placeholder="Correo Electronico   -ljbñivi" name="email" type="text">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <button class="btn btn-primary">Filtrar</button>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                            
                            <table class="table table-striped table-hover dataTable">
                                <thead >
                                    <tr>
                                        <th width="10px">#</th>
                                        <th>Categoria</th>
                                        <th>Nombre</th>
                                        <th>Descripcion</th>
                                        <th>Codigo Barras</th>
                                        <th colspan="3">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->id_categoria }}</td>
                                        <td>{{ $product->nombre }}</td>
                                        <td>{{ $product->descripcion }}</td>
                                        <td>{{ $product->codigo_barras }}</td>
                                        @can('products.show')
                                        <!-- Columna que da la opcion de ver item -->
                                        <td  width="5px">
                                            <button style="background-color:#fff; border:0px solid #fff;">
                                                <a href="{{ route('products.show', $product->id) }}" title="Ver" aria-label="Ver" data-pjax="0">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                            </button>    
                                        </td>

                                        @endcan
                                        @can('products.edit')
                                        <!-- Columna que da la opcion de editar item -->
                                        <td  width="5px">
                                            <button style="background-color:#fff; border:0px solid #fff;">
                                                <a href="{{ route('products.edit', $product->id) }}" title="Editar" aria-label="Editar" data-pjax="0">
                                                    <span  class="glyphicon glyphicon-edit"></span>
                                                </a>
                                            </button>
                                                
                                        </td>

                                        @endcan
                                        @can('products.destroy')
                                        <!--  Columna que da la opcion de eliminar item-->
                                        <td width="5px">
                                            {!! Form::open(['route' => ['products.destroy', $product->id], 
                                            'method' => 'DELETE']) !!}
    
                                            <button style="background-color:#fff; border:0px solid #fff;" id="button-eliminar" title="Eliminar" aria-label="Eliminar" data-pjax="0">
                                                <span style="color: #3c8dbc;" class="glyphicon glyphicon-trash"></span>
                                            </button>
                                                
                                            {!! Form::close() !!}
                                        </td>
                                        @endcan
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $products->render() }}

                        </div>   <!-- fin header --> 
                    </div>                            
                </div>                    
            </div><!-- ./col -->
        </div>
    </section>

@endsection
<script>
    $("#button-eliminar").click(function(){
        alert("boton de eliminar");
    });
</script>