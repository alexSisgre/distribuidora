@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>Empleados
        <small>Crear Empleado</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"><a href="{{ url('empleados') }}">empleados</a></li>
        <li class="active">Crear</li>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <!-- small box -->
                <div class="tab-content">
        
                <div class="box box-default">
                        <div class="box-header with-border">
                        <i class="fa  fa-user-secret"></i>
                        <h3 class="box-title">Crear Empleado.</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    
                    <!-- /.box-header -->
                    <div class="box-body"> 

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#datos_basicos">Datos Basicos</a></li>
                        <li><a data-toggle="tab" href="#menu1">Experiencia Laboral</a></li>
                        <li><a data-toggle="tab" href="#menu2">Formacion Academica</a></li>
                        <li><a data-toggle="tab" href="#menu3">Otros</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="datos_basicos" class="tab-pane fade in active">

                            @include('empleados.fragment.error')
                              
                            <form class="form-group" action="{{ url('/empleados') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="content">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="tipo_documento_id">{{'Tipo Documento'}} <b>*</b></label>
                                            <select class="form-control" name="tipo_documento_id" id="tipo_documento_id">
                                                <option value="">- Elija Opcion -</option>
                                                <option value="1">CC</option>
                                                <option value="2">CE</option>
                                                <option value="3">PS</option>
                                            </select>
                                            @if ($errors->has('tipo_documento_id'))
                                                <span class="help-block" style="color:#a94442">
                                                    <strong>{{ $errors->first('tipo_documento_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="documento">{{'Documento'}} <b>*</b></label>
                                            <input class="form-control" type="number" name="documento" id="documento" value="" placeholder="# de documento">
                                            @if ($errors->has('documento'))
                                                <span class="help-block" style="color:#a94442">
                                                    <strong>{{ $errors->first('documento') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6 ">
                                            <label for="Nombre">{{'Nombre'}} <b>*</b></label>
                                            <input class="form-control" type="text" name="nombre" id="nombre" value="" placeholder="Nombres empleado">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="Apellido">{{'Apellido'}} <b>*</b></label>
                                            <input class="form-control" type="text" name="apellido" id="apellido" value="" placeholder="Apellidos empleado">
                                        </div>
                                        
                                        <div class="form-group col-md-6">
                                            <label for="direccion">{{'Direccion'}} <b>*</b></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa fa-map-marker"></i></span>
                                                <input type="text" class="form-control" type="text" name="direccion" id="direccion" value="" placeholder="Direccion casa">
                                            </div>
                                        </div>

                                        <!--<div class="form-group col-md-6">
                                            <label for="fecha_nacimiento">{{'Fecha Nacimiento'}} <b>*</b></label>
                                            <div class='input-group date' id='datetimepicker8'>
                                                <input type='text' class="form-control" placeholder="Fecha Nacimiento Empleado"/>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>-->

                                        <div class="form-group col-md-6">
                                            <label for="email">{{'Correo Electronico'}} <b>*</b></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input type="email" class="form-control" type="email" name="email" id="email" value="" placeholder="Correo Electronico Empleado">
                                            </div>
                                        </div>
                                        <!--<div class="form-group col-md-6">
                                            <label for="email">{{'Correo Electronico'}} <b>*</b></label>
                                            <input class="form-control" type="email" name="email" id="email" value="" placeholder="Correo Electronico Empleado">
                                        </div>-->

                                        <div class="form-group col-md-6">
                                            <label for="telefono">{{'Telefono'}} </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input type="number" name="telefono" id="telefono" class="form-control"  placeholder="Telefono fijo Empleado">
                                            </div>
                                        </div>    

                                        <div class="form-group col-md-6">
                                            <label for="celular">{{'Celular'}} </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input type="number" name="celular" id="celular" class="form-control"  placeholder="Celular Empleado">
                                            </div>
                                        </div> 

                                        <div class="form-group col-md-3">
                                            <label for="pais">{{'Pais'}} <b>*</b></label>
                                            <select class="form-control" name="pais" id="pais">
                                                <option value="1">Colombia</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="ciudad">{{'ciudad'}} <b>*</b></label>
                                            <select class="form-control" name="ciudad_id" id="ciudad_id">
                                                <option value="1">Bogota</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group col-md-6">
                                            <label for="barrio">{{'Barrio'}}</label>
                                            <input class="form-control" type="text" name="barrio" id="barrio" value="" placeholder="Barrio del empleado">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="fecha_nacimiento">{{'Fecha Nacimiento'}} <b>*</b></label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" name="fecha_nacimiento" id="datepicker" placeholder="AAAA-MM-DD Fecha Nacimiento">
                                            </div>
                                        </div>
                                       
                                        <div class="form-group col-md-6">
                                            <label for="sexo">{{'Genero'}} <b>*</b></label>
                                            <select class="form-control" name="sexo" id="sexo">
                                                <option value="">- Elija Opcion -</option>
                                                <option value="Masculino">Masculino</option>
                                                <option value="Femenino">Femenino</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="estado_civil_id">{{'Estado Civil'}} <b>*</b></label>
                                            <select class="form-control" name="estado_civil_id" id="estado_civil_id">
                                                <option value="">- Elija Opcion -</option>
                                                <option value="1">Soltero</option>
                                                <option value="2">Casado</option>
                                            </select>
                                        </div>
                                       
                                        <div class="form-group col-md-6">
                                            <label for="foto">{{'Foto'}}</label>
                                            <input class="form-control" type="file" name="foto" id="foto" value="">
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="hv_documento">{{'Hoja de vida (Documento)'}}</label>
                                            <input class="form-control" type="file" name="hv_documento" id="hv_documento" value="" placeholder="Adjunte su hoja de vida.">
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="anexos">{{'Anexos'}}</label>
                                            <input class="form-control" type="file" name="anexos" id="anexos" value="" placeholder="Adjunte su hoja de vida.">
                                        </div>
                                        
                                        <div class="form-group col-md-offset-3 col-md-3">
                                            <label>&nbsp;</label>
                                            <input class="form-control btn btn-primary" type="submit" value="Siguiente" >
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="estado_empleado_id" value="1">
                                <input type="hidden" name="creador" value="1">
                                <input type="hidden" name="activo" value="1">
                            </form>
                        </div>

                        <div id="menu1" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <h3>Menu 3</h3>
                            <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        </div>

                        <!-- Ejemplo formulario
                        <div class="form-group col-md-6">
                            {{ Form::label('nombre', 'Nombre') }}
                            {{ Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre']) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('apellido', 'Apellido') }}
                            {{ Form::text('apellido', null, ['class' => 'form-control', 'id' => 'apellido']) }}
                        </div>

                        <div class="form-group col-md-6">
                            {{ Form::label('tipo_documento_id', 'Tipo Documento') }}
                            {{ Form::text('tipo_documento_id', null, ['class' => 'form-control', 'id' => 'tipo_documento_id']) }}
                        </div>

                        <div class="form-group col-md-6">
                            {{ Form::label('documento', 'Número documento') }}
                            {{ Form::text('documento', null, ['class' => 'form-control', 'id' => 'documento']) }}
                        </div>

                        <div class="form-group col-md-12">
                            {{ Form::label('description', 'Descripción') }}
                            {{ Form::textarea('description', null, ['class' => 'form-control']) }}
                        </div>
                        -->
                    </div>        
                </div>
            </div>
        </div>
    </section>  
   
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker8').datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            });
        });
    </script>

@endsection    