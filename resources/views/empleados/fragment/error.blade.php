@if( $errors->any() )
    <br>
    <div class="row">
        <div class="alert alert-danger col-md-10 col-md-offset-1">
            <button type="button" class="close" data-dismiss="alert">
                &times;
            </button>
            <ul>
                @foreach( $errors->all() as $error )
                    <li> {{ $error }} </li>        
                @endforeach
            </ul>
        </div>
    </div>
@endif    