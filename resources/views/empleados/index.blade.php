@extends('layouts.app')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Empleados
        <small>- inicio</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">empleados</li>
        </ol>
    </section>

   
    <!-- Main content -->
    <section class="content">

        <!-- busqueda avanzada (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <!-- small box -->
                <div class="tab-content">
        
                    <div class="" style="background-color:#fff">
                        <div class="box-header with-border">
                            <i class="fa fa-user"></i>
                            <h3 class="box-title">Administración de Empleados.</h3>

                        </div>    
                    
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="col-md-8 ">
                                <div class="box box-primary" >
                                    <div class="box-header with-border">
                                        <i class="fa fa-search"></i>
                                        <h6 class="box-title" style="font-size:14px">Busqueda Avanzada.</h6>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>    
                                
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="form-group col-md-6">
                                            {{ Form::label('name', 'Nombre') }}
                                            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name','placeholder' =>'Nombre Empleado']) }}
                                        </div>

                                        <div class="form-group col-md-6">
                                            {{ Form::label('name', 'Correo Electronico') }}
                                            {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email','placeholder' =>'Correo Electronico']) }}
                                        </div>

                                        <div class="form-group col-md-6">
                                            <button class="btn btn-primary">Buscar</button>
                                        </div>
                                    </div>
                                </div>    
                            </div>

                            <div class="col-md-4">
                                @can('empleados.create')
                                <a href="{{ route('empleados.create') }}" 
                                    class="btn btn-success pull-right" >
                                    Crear Empleado
                                </a>
                                @endcan
                            </div>

                            <!--<table class="table table-striped table-hover dataTable" role="grid">-->
                            <table id="dataTable" class="table table-striped table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th width="10px">ID</th>
                                        <th>Documento</th>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Correo Electronico</th>
                                        <th>Fecha Ingreso</th>
                                        <th colspan="3" align="center">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($empleados as $empleado)
                                    <tr>
                                        <!-- {{ $loop->iteration }} -->
                                        <td>{{ $empleado->id }}</td>
                                        <td>{{ $empleado->documento }}</td>
                                        <td>{{ $empleado->nombre }}</td>
                                        <td>{{ $empleado->apellido }}</td>
                                        <td>{{ $empleado->email }}</td>
                                        <td>{{ $empleado->fcreador }}</td>
                                        @can('empleados.show')
                                        <!-- Columna que da la opcion de ver item -->
                                        <td  width="3px">
                                            <button class="button-form" style="background-color:#fff; border:0px solid #fff;">
                                                <a href="{{ route('empleados.show', $empleado->id) }}" title="Ver" aria-label="Ver" data-pjax="0">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                            </button>    
                                        </td>
                                        @endcan
                                        @can('empleados.edit')
                                        <!-- Columna que da la opcion de editar item -->
                                        <td  width="3px">
                                            <button class="button-form" style="background-color:#fff; border:0px solid #fff;">
                                                <a href="{{ route('empleados.edit', $empleado->id) }}" title="Editar" aria-label="Editar" data-pjax="0">
                                                    <span  class="glyphicon glyphicon-edit"></span>
                                                </a>
                                            </button>
                                                
                                        </td>
                                        @endcan
                                        @can('empleados.destroy')
                                        <!--  Columna que da la opcion de eliminar item-->
                                        <td width="3px">
                                            {!! Form::open(['route' => ['empleados.destroy', $empleado->id], 
                                            'method' => 'DELETE']) !!}

                                            <button style="background-color:#fff; border:0px solid #fff;" class="button-form" title="Eliminar" aria-label="Eliminar" data-pjax="0">
                                                <span style="color: #3c8dbc;" class="glyphicon glyphicon-trash"></span>
                                            </button>
                                                
                                            {!! Form::close() !!}
                                        </td>
                                        @endcan
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $empleados->render() }}  
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </section>                       

<!-- page script -->

@endsection