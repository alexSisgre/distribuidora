@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Producto
        <small>- Crear Producto</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"><a href="{{ url('products') }}">productos</a></li>
        <li class="active">Crear</li>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <!-- small box -->
                <div class="tab-content">
        
                <div class="box box-default">
                        <div class="box-header with-border">
                        <i class="fa "></i>
                        <h3 class="box-title">Crear Producto.</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    
                    <!-- /.box-header -->
                    <div class="box-body">
                        {{ Form::open(['route' => 'products.store']) }}

                            @include('products.partials.form')

                        {{ Form::close() }}
                    
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection