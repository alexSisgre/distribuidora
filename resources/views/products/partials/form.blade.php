<div class="form-group col-md-6" >
	<label for="name">Categoria</label>
		<!--<input class="form-control" id="name" placeholder="Nombre de Categoria" name="name" type="text">-->
		<select class="form-control" name="id_categoria" focus>
			<option value="" disabled selected>[-- Seleccione Categoria --]</option>        
			@foreach($categorias as $categoria)
				<option value="{{$categoria->id}}">{{ $categoria->nombre }}</option>
			@endforeach
		</select>
</div>
<div class="form-group col-md-6" >
	{{ Form::label('nombre', 'Nombre del Producto') }}
	{{ Form::text('nombre', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Ingrese nombre del producto']) }}
</div>
<div class="form-group col-md-6">
	{{ Form::label('codigo_barras', 'Codigo de barras del Producto') }}
	{{ Form::text('codigo_barras', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Escanee el codigo de barras']) }}
</div>
<div class="form-group col-md-6">
	{{ Form::label('precio', 'Precio del Producto') }}
	{{ Form::text('precio', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Ingrese el precio del producto']) }}
</div>
<div class="form-group col-md-6">
	{{ Form::label('stock', 'stock') }}
	{{ Form::text('stock', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Valor de Stock Inicial']) }}
</div>
<div class="form-group col-md-6">
	{{ Form::label('descripcion', 'Descripción') }}
	{{ Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Ingrese una descripcion que relacione al producto']) }}
</div>
<div class="form-group col-md-6">
	{{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>