@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Roles
        <small>- inicio</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">roles</li>
        </ol>
    </section>

 
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <!-- small box -->
                <div class="tab-content">
        
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa  fa-user-secret"></i>
                            <h3 class="box-title">Gestion de Roles.</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        
                        <!-- /.box-header -->
                        <div class="box-body">    
                            @can('roles.create')
                            <a href="{{ route('roles.create') }}" 
                                class="btn btn-success pull-right">
                                Crear Rol
                            </a>
                            @endcan
                            
                            <table class="table table-striped table-hover dataTable">
                                <thead >
                                    <tr>
                                        <th width="10px">#</th>
                                        <th>Nombre</th>
                                        <th>Descripcion</th>
                                        <th colspan="3">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $role->id }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>{{ $role->description }}</td>
                                        @can('roles.show')
                                        <!-- Columna que da la opcion de ver item -->
                                        <td  width="5px">
                                            <button style="background-color:#fff; border:0px solid #fff;">
                                                <a href="{{ route('roles.show', $role->id) }}" title="Ver" aria-label="Ver" data-pjax="0">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                            </button>    
                                        </td>

                                        @endcan
                                        @can('roles.edit')
                                        <!-- Columna que da la opcion de editar item -->
                                        <td  width="5px">
                                            <button style="background-color:#fff; border:0px solid #fff;">
                                                <a href="{{ route('roles.edit', $role->id) }}" title="Editar" aria-label="Editar" data-pjax="0">
                                                    <span  class="glyphicon glyphicon-edit"></span>
                                                </a>
                                            </button>
                                                
                                        </td>

                                        @endcan
                                        @can('roles.destroy')
                                        <!--  Columna que da la opcion de eliminar item-->
                                        <td width="5px">
                                            {!! Form::open(['route' => ['roles.destroy', $role->id], 
                                            'method' => 'DELETE']) !!}
    
                                            <button style="background-color:#fff; border:0px solid #fff;"  title="Eliminar" aria-label="Eliminar" data-pjax="0">
                                                <span style="color: #3c8dbc;" class="glyphicon glyphicon-trash"></span>
                                            </button>
                                                
                                            {!! Form::close() !!}
                                        </td>
                                        @endcan
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $roles->render() }}

                        </div>   <!-- fin header --> 
                    </div>                            
                </div>                    
            </div><!-- ./col -->
        </div>
    </section>

@endsection