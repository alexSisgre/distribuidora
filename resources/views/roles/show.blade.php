@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Roles
        <small>vista de rol</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"><a href="{{ url('roles') }}">roles</a></li>
        <li class="active">ver</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
        <br>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    
                    <div class="panel-heading"><i class="fa  fa-user-secret"></i> &nbsp;<b>Rol</b></div>

                    <div class="panel-body">                                        
                        <p><strong>Nombre</strong>&nbsp;&nbsp;     {{ $role->name }}</p>
                        <p><strong>Slug</strong>&nbsp;&nbsp;       {{ $role->slug }}</p>
                        <p><strong>Descripción</strong>&nbsp;&nbsp;  {{ $role->description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection