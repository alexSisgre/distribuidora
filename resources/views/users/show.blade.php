@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Usuarios
        <small>- inicio</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"><a href="{{ url('users') }}">Usuarios</a></li>
        <li class="active">Ver</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- busqueda avanzada (Stat box) -->
        <div class="row">        
            <div class="col-lg-12">
                <!-- small box -->
                <div class="tab-content">
                    <div class="box box-default">
                        <div class="box-header with-border">
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Vista de Usuario.</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    
                        <table class="table table-striped table-hover dataTable">
                            <thead>
                                <tr>
                                    <th width="10px">ID</th>
                                    <th>Nombre</th>
                                    <th>E-mail</th>
                                    <td>Demas datos</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>                            


@endsection